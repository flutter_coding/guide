# Coding Standards and Style Guide Version: 1.0.0

<br>

<div style="text-align:center"><img src="images/monkey-coding.png" /></div>

<br>

**Overview**: This document describes coding standards and style rules for writing **Dart** and **Flutter** projects.

**_TL;DR: The goal is readable, consistent and maintainable code._**

Don't optimize for the size of the source code on disk, nor how long it takes to type the program into an editor. Rather, optimise the code to be clear to the reader. Because its the reader who has to maintain the code in the future, and that reader could be you or worst, it could be me...

<br>
<br>

\* The cover image is from the amazing book [Category Theory for Programmers](https://github.com/hmemcpy/milewski-ctfp-pdf) by Bartosz Milewski licensed under the Creative Commons Attribution-ShareAlike 4.0 International License [cc by-sa 4.0](http://creativecommons.org/licenses/by-sa/4.0/).
<div style="page-break-after: always;"></div>

Table of Contents

<!-- TOC -->

- [Coding Standards and Style Guide Version: 1.0.0](#coding-standards-and-style-guide-version-100)
  - [1. Documentation](#1-documentation)
    - [1.1. Comments](#11-comments)
    - [1.2. README](#12-readme)
    - [1.3. CHANGELOG](#13-changelog)
  - [2. Versioning](#2-versioning)
  - [3. Naming](#3-naming)
  - [4. Lint Rules](#4-lint-rules)
    - [4.1. Lint Setup](#41-lint-setup)
  - [5. Dart style guide](#5-dart-style-guide)
    - [5.1. Additional Style Rules](#51-additional-style-rules)
    - [5.2. Additional Usage Rules](#52-additional-usage-rules)
      - [5.2.1. Repositories and Files](#521-repositories-and-files)
    - [5.3. Coding practices](#53-coding-practices)
    - [5.4. Additional Design Rules](#54-additional-design-rules)
  - [6. a11y & i18n](#6-a11y--i18n)
    - [6.1. Accessibility](#61-accessibility)
      - [6.1.1. Accessibility release checklist](#611-accessibility-release-checklist)
    - [6.2. Internationalization](#62-internationalization)
  - [7. Architecture](#7-architecture)
    - [7.1. Project Structure](#71-project-structure)
    - [7.2. Layered Architecture](#72-layered-architecture)
      - [7.2.1. Presentation Layer](#721-presentation-layer)
      - [7.2.2. Application Layer](#722-application-layer)
      - [7.2.3. Domain Layer](#723-domain-layer)
      - [7.2.4. Infrastructure Layer](#724-infrastructure-layer)
    - [7.3. State Management](#73-state-management)
    - [7.4. Widgets](#74-widgets)
  - [8. Testing](#8-testing)
  - [9. Security](#9-security)

<!-- /TOC -->

<div style="page-break-after: always;"></div>

The guidelines starts with one of these words:

- **DO** guidelines describe practices that should always be followed. There will almost never be a valid reason to stray from them.

- **DON’T** guidelines are the converse: things that are almost never a good idea. Hopefully, we don’t have as many of these as other languages do because we have less historical baggage.

- **PREFER** guidelines are practices that you should follow. However, there may be circumstances where it makes sense to do otherwise. Just make sure you understand the full implications of ignoring the guideline when you do.

- **AVOID** guidelines are the dual to “prefer”: stuff you shouldn’t do but where there may be good reasons to on rare occasions.

- **CONSIDER** guidelines are practices that you might or might not want to follow, depending on circumstances, precedents, and your own preference.

## 1. Documentation

It’s easy to think your code is obvious today without realizing how much you rely on context already in your head. People new to your code, and even your forgetful future self won’t have that context. A concise, accurate comment only takes a few seconds to write but can save hours of time in the future.

- **DO** use **english language** for all comments and documentation.

### 1.1. Comments

- **DO** adhere all comments to [Effective Dart: Documentation](https://dart.dev/guides/language/effective-dart/documentation).

### 1.2. README

- **DO** include a `README.md` file in the root of the project to provide orientation for engineers browsing your code and any documentation needed for building, running or preparing any assets or resources needed.

  At minimum, the `README.md` file should include or point to the following information:

  - What is in this application/package/library and what's it used for.
  - Where to go for more detailed information, any external or included documentation files.
  - Any API documentation for using this package/library.
  - Any information of external services needed for integration/running/testing.
  - Any commands or scripts for building, localization, code generation, testing, etc.

### 1.3. CHANGELOG

- **DO** keep a `CHANGELOG.md` file in the root of the project to describe all the changes made.

  The purpose of this file is to log the changes of the project, like new features added, API or contract changes.

  More importantly, people investigating bugs in the future will know about the changes that might have introduced the bug. Often a new bug can be found by looking at what was recently changed. Therefore, change logs should be detailed enough and accurate enough to provide the information commonly required for such software forensics.

## 2. Versioning

- **DO** use [Semantic Versioning 2.0.0](https://semver.org/) for versioning the project and/or packages.

## 3. Naming

In General, choose names that are meaningful and readable.  If the name is appropriate everything fits together naturally, relationships are clear, meaning is derivable, and reasoning from common human expectations works as expected.

- **DO** use **english language** for any name.

- **DO** follow the guidelines in [Effective Dart](https://www.dartlang.org/guides/language/effective-dart) specially [Effective Dart Design Names](https://dart.dev/guides/language/effective-dart/design#names).

- **DO** follow the guidelines in [BLoC Naming Conventions](https://bloclibrary.dev/#/blocnamingconventions) for `events` and `states` naming.

## 4. Lint Rules

- **DO** follow the lint rules specified by the `analysis_options.yaml` file defined below.

  These lint rules will help to automate the review process. They are based on [lint package rules](https://pub.dev/packages/lint), that also follows the [Effective Dart: Style Guide](https://dart.dev/guides/language/effective-dart/style).

- **DO** make the code pass all the lint rules. There are situations where a lint rule may be in conflict with a specific API and may need to be explicitly ignored. If a file is opting out of a lint rule the developer must provide a comment explaining the reasoning for opting out of the lint rule.

### 4.1. Lint Setup

Add `lint` as dependency to your `pubspec.yaml`:

```yaml
dev_dependencies:
  lint: ^1.0.0
```

Create an `analysis_options.yaml` file in the root of your project and import the `lint` rules:

```yaml
include: package:lint/analysis_options.yaml

analyzer:
  errors:
    missing_required_param: error
    missing_return: error
    must_be_immutable: error
    parameter_assignments: error
    todo: ignore
  strong-mode:
    implicit-casts: false
    implicit-dynamic: false
  exclude:
    - "build/**"
    - "**/*.g.dart"
    - "**/*.gr.dart"
    - "**/*.freezed.dart"
    - "**/generated/**"
linter:
  rules:
    prefer_relative_imports: true
    sort_constructors_first: true
```

If you're writing a package with a public API you should use the `package` version instead:

```yaml
include: package:lint/analysis_options_package.yaml
```

## 5. Dart style guide

- **DO** format all code using `dartfmt` before being checked in. Formatting all code with this tool will ensure consistency.

- **DO** follow the guidelines in [Effective Dart](https://www.dartlang.org/guides/language/effective-dart), but with some additions.

### 5.1. Additional Style Rules

- **DO** use trailing commas on all tree structures longer than one line.

  Without trailing commas, code that builds widget trees or similar types of code tends to be hard to read. Adding the trailing commas allows `dartfmt` to do its job correctly.

  Without trailing commas:

  ```dart
  children.add(Expanded(
    child: Center(
        child: Container(
            width: 64.0, height: 64.0, child: FuchsiaSpinner())),
  ));
  ```

  With trailing commas:

  ```dart
  children.add(Expanded(
    child: Center(
        child: Container(
          width: 64.0,
          height: 64.0,
          child: FuchsiaSpinner(),
        ),
    ),
  ));
  ```

- **DO** order members using the `Flutter Stylizer`.

  In **Visual Studio Code** use the [Flutter Stylizer](https://github.com/gmlewis/flutter-stylizer) extension to organize the Flutter classes in an opinionated and consistent manner.

  VSCode User Preferences:

  ```json
  "flutterStylizer.memberOrdering": [
      "public-constructor",
      "named-constructors",
      "public-static-variables",
      "public-instance-variables",
      "private-static-variables",
      "private-instance-variables",
      "public-override-methods",
      "public-other-methods",
      "build-method",
  ],
  ```

- **PREFER** to keep lines below 80 characters unless it would be more readable.

  It is fine to have lines above 80 characters, as long as it improves readability, for example with URLs, and `dartfmt` won't automatically truncate the line.

### 5.2. Additional Usage Rules

#### 5.2.1. Repositories and Files

- **DO** import all files within a package using relative paths.

  Mixing and matching relative and absolute paths within a single package causes Dart to act as if there were two separate imports of identical files, which will introduce errors in type checking. Either format works as long as it is consistent, but do use relative paths. This does not apply to external libraries, as only the absolute path can be used.

  When importing a library file from your own package, use a relative path when both files are inside of `lib`, or when both files are outside of `lib`.

  To import `lib/foo/a.dart` from inside of `lib`:

  ```dart
  import '../foo/a.dart';
  ```

  Use `package:` when the imported file is in `lib` and the importer is outside.

  When importing a library file from another package, use the the `package:` directive to specify the URI of that file.

  ```dart
  import 'package:utilities/utilities.dart';
  ```

- **PREFER** minimizing the number of public members exposed in a package.

  This can be done by only making things public when needed, and keeping all implementation detail libraries in the `/src` directory. Assume anything public in the `lib` directory will be re-used.

- **CONSIDER** exporting publicly visible classes in a single .dart file.

  For multiple classes that are used together but are in different files, it’s more convenient for users of your library to import a single file rather many at once. If the user wants narrower imports they can always restrict visibility using the `show` keyword.

  This also helps minimize the publicly visible surface.

  Example:

  ```dart
  /// In src/apple.dart
  class Apple {}

  /// In src/orange.dart
  class Orange {}

  /// In src/veggies.dart
  class Potato {}
  class Tomato {}

  /// In botanical_fruits.dart
  export 'src/apple.dart';
  export 'src/orange.dart';
  // Can also be: export 'src/veggies.dart' hide Potato;
  export 'src/veggies.dart' show Tomato;

  /// In squeezer.dart
  import '../plants/botanical_fruits.dart' show Orange;
  ```

- **DO** use namespacing when there is ambiguity, e.g. in class names.

  There are often functions or classes that can collide, e.g. `File` or `Image`. If you don't namespace, there will be a compile error.

  Good:

  ```dart
  import 'dart:ui' as ui;

  import 'package:flutter/material.dart';
  ...

  ui.Image(...) ...
  ```

  Bad:

  ```dart
  import 'dart:ui';

  import 'package:flutter/material.dart';
  ...

  Image(...) ... // Which Image is this?
  ```

- **PREFER** to use show if you only have a few imports from that package. Otherwise, use as.

  Using `show` can avoid collisions without requiring you to prepend namespaces to types, leading to cleaner code.

  Good:

  ```dart
  import 'package:fancy_style_guide/style.dart' as style;
  import 'package:flutter/material.dart';
  import 'package:math/simple_functions.dart' show Addition, Subtraction;
  ```

  Bad:

  ```dart
  import 'package:flutter/material.dart show Container, Row, Column, Padding,
    Expanded, ...;
  ```

### 5.3. Coding practices

- **DON'T** use new or use const redundantly.

  Dart 2 makes the `new` optional for constructors, with an aim at removing them in time. The `const` keyword is also optional where it can be inferred by the compiler.

  `const` can be inferred in:

  * A const collection literal.
  * A const constructor call.
  * A metadata annotation.
  * The initializer for a const variable declaration.
  * A switch case expression — the part right after case before the :, not the body of the case.

  This guidance will eventually be part of Effective Dart due to the changes for Dart 2.

  Good:

  ```dart
  final foo = Foo();
  const foo = Foo();
  const foo = const <Widget>[A(), B()];
  const Foo(): bar = Bar();
  ```

  Bad:

  ```dart
  final foo = new Foo();
  const foo = const Foo();
  foo = const [const A(), const B()];
  const Foo(): bar = const Bar();
  ```

- **DON'T** do useful work in assert statements.

  Code inside assert statements are not executed in production code. Asserts should only check conditions and be side-effect free.

- **PREFER** to use const over final over var.

  This minimizes the mutability for each member or local variable.

- **PREFER** return Widget instead of a specific type of Flutter widget.

  As your project evolves, you may change the widget type that is returned in your function. For example, you might wrap your widget with a Center. Returning `Widget` simplifies the refactoring, as the method signature wouldn't have to change.

  Good:

  ```dart
  Widget returnContainerWidget() {
    return Container();
  }
  ```

  Bad:

  ```dart
  Container returnContainerWidget() {
    return Container();
  }
  ```

### 5.4. Additional Design Rules

- **AVOID** mixing named and positional parameters.

  Instead, `@required` should be used in place of required positional parameters.

- **PREFER** named parameters.

  In most situations, named parameters are less error prone and easier to read than positional parameters, optional or not. They give users to pass in the parameters in whatever order they please, and make Flutter trees especially clearer.

  Positional parameters should be reserved for simple operational functions with only a few parameters.

  Good:

  ```dart
  int add(int a, int b);
  int addNumbers(int a, [int b, int c, int d]);
  Foo fromJson(String json);
  void load(String file);

  Widget buildButton({
    @required Widget child,
    VoidCallback onTap,
    double width,
    bool isDisabled = false,
  });
  ```

  Bad:

  ```dart
  int add({int a, int b});
  Foo fromJson({@required String json});

  Widget buildButton(
    Widget child,
    VoidCallback onTap, [
    double width,
    bool isDisabled = false,
  ]);
  ```

## 6. a11y & i18n

### 6.1. Accessibility

Ensuring applications are accessible to a broad range of users is an essential part of building a high-quality app. Applications that are poorly designed create barriers to people of all ages.

- **DO** follow the guidelines in [Flutter Accessibility](https://flutter.dev/docs/development/accessibility-and-localization/accessibility).

- **DO** inspect accessibility support with [Accessibility Scanner](https://play.google.com/store/apps/details?id=com.google.android.apps.accessibility.auditor&hl=en) for (Android) or Xcode Accessibility Inspector (iOS).

#### 6.1.1. Accessibility release checklist

Here is a non-exhaustive list of things to consider for app release:

  - Active interactions. Ensure that all active interactions do something. Any button that can be pushed should do something when pushed. For example, if you have a no-op callback for an `onPressed` event, change it to show a `SnackBar` on the screen explaining which control you just pushed.
  - Screen reader testing. The screen reader should be able to describe all controls on the page when you tap on them, and the descriptions should be intelligible. Test your app with [TalkBack](https://support.google.com/accessibility/android/answer/6283677?hl=en) (Android) and [VoiceOver](https://www.apple.com/lae/accessibility/iphone/vision/) (iOS).
  - Contrast ratios. We encourage you to have a contrast ratio of at least 4.5:1 between controls or text and the background, with the exception of disabled components. Images should also be vetted for sufficient contrast.
  - Context switching. Nothing should change the user’s context automatically while typing in information. Generally, the widgets should avoid changing the user’s context without some sort of confirmation action.
  - Tappable targets. All tappable targets should be at least 48x48 pixels.
  - Errors. Important actions should be able to be undone. In fields that show errors, suggest a correction if possible.
  - Color vision deficiency testing. Controls should be usable and legible in colorblind and grayscale modes.
  - Scale factors. The UI should remain legible and usable at very large scale factors for text size and display scaling.

### 6.2. Internationalization

Localization is the process of making your app available to other locales. It includes the parameters defining the user’s language, region and any particular variant — for example, en-US (American English).

Internationalization is how you architect and engineer your code to make it easy to localize.

- **DO** internationalize even if the app supports just one locale, so we have the capacity to ultimately support more than one locale without dramatic engineering efforts. It’ll make the code cleaner and more future-proof.

- **DO** define `US English` as the first element of the `supportedLocales` list, so it's the default fallback supported locale.

- **DO** use the [intl](https://pub.dev/packages/intl) package following the guidelines in [Flutter ](https://flutter.dev/docs/development/accessibility-and-localization/internationalization).

- **CONSIDER** using [Flutter Intl](https://marketplace.visualstudio.com/items?itemName=localizely.flutter-intl) VSCode extension to generate boilerplate code.

- **DO** use [NumberFormat](https://pub.dev/documentation/intl/latest/intl/NumberFormat-class.html) if needed to provides the ability to format numbers in a locale-specific way.

- **DO** use **english language** for naming the text entries.

- **DO** prefix de string entries names with the place they appear in the app.

  Example:

  ```dart
  {
    "@@locale": "en_US",
    "formPageAppBarTitle": "Awesome App",
    "formPageActionButtonTitle": "Awesomize",
    "resultsPageAppBarTitle": "Awesomnator Results",
  }
  ```

- **DON'T** reuse text entries, which is why they were named after the place they appear in the app. The same string can have different translations depending on its context.

> Awesome explanation: [Internationalizing and Localizing Your Flutter App](https://www.raywenderlich.com/10794904-internationalizing-and-localizing-your-flutter-app).

## 7. Architecture

### 7.1. Project Structure

- **DO** use a folder structure based on a `Domain-Driven Design` (DDD), where the `layers` folders (application/domain/infrastructure/presentation) will hold `feature` folders and possibly a `core` folder which holds classes common to all the features in that layer.

  The most important thing is having a consistent and intentional project structure. This will keep the code readable but, most importantly, it will ensure that adding more features and sub-features could be done in a controlled way, even mixing and matching dependencies between features, but keeping true the responsibilities of each layer.

  ```sh
  lib
  ├── application
  │   ├── auth
  │   │   ├── auth_bloc.dart
  │   │   ├── auth_event.dart
  │   │   ├── auth_state.dart
  │   │   └── sign_in_form
  │   │       ├── sign_in_form_bloc.dart
  │   │       ├── sign_in_form_event.dart
  │   │       └── sign_in_form_state.dart
  │   └── notes
  ├── domain
  │   ├── auth
  │   ├── core
  │   └── notes
  ├── infrastructure
  │   ├── auth
  │   ├── core
  │   └── notes
  ├── presentation
  │   ├── app_builder.dart
  │   ├── pages
  │   │   ├── notes
  │   │   │   ├── note_form
  │   │   │   │   ├── note_form_page.dart
  │   │   │   │   └── widgets
  │   │   │   └── notes_overview
  │   │   │       ├── notes_overview_page.dart
  │   │   │       └── widgets
  │   │   ├── sign_in
  │   │   │   ├── sign_in_page.dart
  │   │   │   └── widgets
  │   │   │       └── sign_in_form.dart
  │   │   └── splash
  │   │       └── splash_page.dart
  │   └── routes
  │       └── router.dart
  └── main.dart
  ```

  > Awesome explanation: [Flutter Firebase DDD Course 1](https://resocoder.com/2020/03/09/flutter-firebase-ddd-course-1-domain-driven-design-principles/) and [Example](https://github.com/ResoCoder/finished-flutter-firebase-ddd-course).

- **DO** follow the recommendations laid out by the [Effective Dart](https://dart.dev/tools/pub/package-layout) style guide, taking in count how the code is exported, if the project or part of it needs to be defined as a Dart package.

  For a more complicated package, avoid a singular catch all top-level export file and rather expose a top level file per logical grouping of classes that make sense to be pulled under one import line. This allows users of the library the ability to have finer grained control over which sections of the library they import. An example is a package which contains functionality for both agents and modules. In this scenario, we could have one import for agents and one for modules but they could be in the same package.

### 7.2. Layered Architecture

#### 7.2.1. Presentation Layer

This layer is all widgets and routes, totally dependent on the Flutter framework. The `presentation` layer's responsibility is to figure out how to render itself based on one or more `BLoC` `states`, and handle user input and application lifecycle `events`.

It's important to remember that `events` and `states` are logically a part of the `presentation` layer. It is, therefore, fine if they're tightly coupled to what's going on in the Flutter `widgets`.

> Note that `event` and `state` files are defined with `freezed` and located in the `application` layer side by side with the `BLoC` files.

- **DON’T** do validation inside the `presentation` layer. Validation is done by `ValueObjects` in the `domain` layer.

- **DO** use [BlocConsumer](https://bloclibrary.dev/#/flutterbloccoreconcepts?id=blocconsumer) if a listener and builder are needed, for example when showing an `SnackBar` in response to a `state` change in a `BLoC`. `BlocConsumer` is analogous to a nested `BlocListener` and `BlocBuilder` but reduces the amount of boilerplate needed.

- **DO** use an `AppWidget` to support changing top level settings such as the application theme or locale by reacting to an `AppBloc` and rebuilding the `MaterialApp` without affecting the `state`.

- **DO** use a `MultiBlocProvider` in the `AppWidget` to expose the `BLoCs` even if there is only one `BlocProvider` for the moment.

#### 7.2.2. Application Layer

The role of the `application` layer is to decide what to do next with the data. It doesn't perform any complex business logic, instead, make use of `BLoCs` to orchestrate all of the other layers to work together by transforming incoming `events` into `state`.

It's important to remember that `events` and `states` are a part of the `presentation` layer. It is, therefore, fine if they're tightly coupled to what's going on in the Flutter `widgets`.

- **DO** use [flutter_bloc](https://pub.dev/packages/flutter_bloc) to implement the `BLoC` (Business Logic Component) design pattern for state management and map the `events` to `state`.

- **DO** use the injected implementations of the `facade` interfaces defined in the `domain` layer to interact with the `repositories`   defined in the `infrastructure` layer.

  Example `lib/application/auth/sign_in_form/sign_in_form_bloc.dart`:

  ```dart
  {
    @injectable
    class SignInFormBloc extends Bloc<SignInFormEvent, SignInFormState> {
      final IAuthFacade _authFacade;

      SignInFormBloc(this._authFacade);

      @override
      SignInFormState get initialState => SignInFormState.initial();

      @override
      Stream<SignInFormState> mapEventToState(
        SignInFormEvent event,
      ) async* {
        yield* event.map(
          emailChanged: (e) async* {
            yield state.copyWith(
              emailAddress: EmailAddress(e.emailStr),
              authFailureOrSuccessOption: none(),
            );
          },
          ...
          signInWithEmailAndPasswordPressed: (e) async* {
            yield* _performActionOnAuthFacadeWithEmailAndPassword(
              _authFacade.signInWithEmailAndPassword,
            );
          },
          signInWithGooglePressed: (e) async* {
            yield state.copyWith(
              isSubmitting: true,
              authFailureOrSuccessOption: none(),
            );
            final failureOrSuccess = await _authFacade.signInWithGoogle();
            yield state.copyWith(
                isSubmitting: false,
                authFailureOrSuccessOption: some(failureOrSuccess));
          },
        );
    }

    Stream<SignInFormState> _performActionOnAuthFacadeWithEmailAndPassword(
      Future<Either<AuthFailure, Unit>> Function({
        @required EmailAddress emailAddress,
        @required Password password,
      })
          forwardedCall,
    ) async* {
      Either<AuthFailure, Unit> failureOrSuccess;

      final isEmailValid = state.emailAddress.isValid();
      final isPasswordValid = state.password.isValid();

      if (isEmailValid && isPasswordValid) {
        yield state.copyWith(
          isSubmitting: true,
          authFailureOrSuccessOption: none(),
        );

        failureOrSuccess = await forwardedCall(
          emailAddress: state.emailAddress,
          password: state.password,
        );
      }
      yield state.copyWith(
        isSubmitting: false,
        showErrorMessages: true,
        authFailureOrSuccessOption: optionOf(failureOrSuccess),
      );
  }
  ```

#### 7.2.3. Domain Layer

This layer is where business logic lives, is fully self contained and it doesn't depend on any other layer, nor anything external. Changes to implementation details should not affect this layer. On the other hand, all the other layers do depend on the `domain` layer.

It handles: Validating data and keeping it valid with `ValueObjects`, transforming data, grouping and uniquely identifying data that belongs together through `Entity` classes, and also is the home of `Failures` union types.

- **DO** use `ValueObjects` for validating data and keeping it valid. For example, instead of using a plain `String` for an email address, we're going to have a separate class called `EmailAddress`. It will encapsulate a `String` value and make sure that it's a valid email and that it's not empty.

  Example `lib/domain/auth/value_objects.dart`:

  ```dart
  class EmailAddress extends ValueObject<String> {
    @override
    final Either<ValueFailure<String>, String> value;

    factory EmailAddress(String input) {
      assert(input != null);
      return EmailAddress._(
        validateEmailAddress(input),
      );
    }

    const EmailAddress._(this.value);
  }
  ```

  > Awesome explanation: [Flutter Firebase DDD Course 2](https://resocoder.com/2020/03/13/flutter-firebase-ddd-course-2-authentication-value-objects/).

- **DO** use `Entity` classes for grouping and uniquely identifying data that belongs together (e.g. User or Note entities).

  Example `lib/domain/notes/note.dart`:

  ```dart
  @freezed
  abstract class Note with _$Note implements IEntity {
    const factory Note({
      @required UniqueId id,
      @required NoteBody body,
      @required NoteColor color,
      @required List3<TodoItem> todos,
    }) = _Note;

    factory Note.empty() => Note(
          id: UniqueId(),
          body: NoteBody(''),
          color: NoteColor(NoteColor.predefinedColors[0]),
          todos: List3(emptyList()),
        );
  }
  ```

- **DO** create `Failures` unions grouped with [freezed](https://pub.dev/packages/freezed) to encapsulate the `exceptions`.

  Example `lib/domain/notes/note_failure.dart`:

  ```dart
  @freezed
  abstract class NoteFailure with _$NoteFailure {
    const factory NoteFailure.unexpected() = Unexpected;
    const factory NoteFailure.insufficientPermissions() = InsufficientPermissions;
    const factory NoteFailure.unableToUpdate() = UnableToUpdate;
  }
  ```

- **DO** define `facade` interfaces to express the desire functionality that needs to be implemented by the `repositories` in the `infrastructure` layer. These `facades` fulfills the `DDD` specification where the `application` layer cannot depend on classes from the `infrastructure` layer, making the `repositories` an implementation detail.

  Example `lib/domain/auth/i_auth_facade.dart`:

  ```dart
  abstract class IAuthFacade {
    Future<Option<User>> getSignedInUser();
    Future<Either<AuthFailure, Unit>> registerWithEmailAndPassword({
      @required EmailAddress emailAddress,
      @required Password password,
    });
    Future<Either<AuthFailure, Unit>> signInWithEmailAndPassword({
      @required EmailAddress emailAddress,
      @required Password password,
    });
    Future<Either<AuthFailure, Unit>> signInWithGoogle();
    Future<void> signOut();
  }
  ```

- **DO** use the strong naming convention `i_<GROUPING>_facade.dart` for the file name of the defined `facade` interfaces.

- **DO** use the strong naming convention `I<GROUPING>Facade` for the defined `facade` interfaces.

  > Awesome explanation: [Flutter Firebase DDD Course 3](https://resocoder.com/2020/03/17/flutter-firebase-ddd-course-3-auth-facade-interface/).

- **DO** use `Either` from the [dartz](https://pub.dev/packages/dartz) package to return either a `value` or a `failure`.

- **DO** use `Unit` from the [dartz](https://pub.dev/packages/dartz) package as the void return value when returning a `failure`.

- **DO** use [freezed](https://pub.dev/packages/freezed) package to group all `failures` from validated `ValueObjects` into one `ValueFailure` union. Since this is something common across features, create the `failures.dart` file inside the `domain/core` folder.

  Example:

  ```dart
  @freezed
  abstract class ValueFailure<T> with _$ValueFailure<T> {
    const factory ValueFailure.empty({
      @required T failedValue,
    }) = Empty<T>;
    const factory ValueFailure.invalidEmail({
      @required T failedValue,
    }) = InvalidEmail<T>;
    const factory ValueFailure.shortPassword({
      @required T failedValue,
    }) = ShortPassword<T>;
  }
  ```

  > Awesome explanation: [Flutter Firebase DDD Course 2](https://resocoder.com/2020/03/13/flutter-firebase-ddd-course-2-authentication-value-objects/).

#### 7.2.4. Infrastructure Layer

The `infrastructure` layer is at the other extreme of the `presentation` layer, it deals with APIs, external services, databases and device sensors. This layer is composed of `repositories`, `data sources` and `data transfer objects`.

- **DO** use `data transfer objects` (DTOs) to convert data between `entities` and `ValueObjects` from the `domain` layer and the plain data of the outside world.

- **DO** use `remote and local data sources` as the lowest level to connect with external raw data. For example use `remote data sources` to fit `JSON` responses from a server into `DTOs`, and perform server requests with `DTOs` converted to `JSON`. Similarly, `local data sources` fetch data from a local database or from the device sensors.

- **DO** use `repositories` as boundary between the `domain` / `application` layers and the outside world, by implementing the `facade interfaces` defined in the `domain` layer and by transforming the `exceptions` into predictable results.

  Example `lib/infrastructure/auth/firebase_auth_facade.dart`:

  ```dart
  @prod
  @lazySingleton
  @RegisterAs(IAuthFacade)
  class FirebaseAuthFacade implements IAuthFacade {
    final FirebaseAuth _firebaseAuth;
    final GoogleSignIn _googleSignIn;

    FirebaseAuthFacade(
      this._firebaseAuth,
      this._googleSignIn,
    );

    @override
    Future<Either<AuthFailure, Unit>> signInWithEmailAndPassword({
      @required EmailAddress emailAddress,
      @required Password password,
    }) async {
      final emailAddressStr = emailAddress.value.getOrElse(() => 'INVALID EMAIL');
      final passwordStr = password.value.getOrElse(() => 'INVALID PASSWORD');
      try {
        return await _firebaseAuth
            .signInWithEmailAndPassword(
              email: emailAddressStr,
              password: passwordStr,
            )
            .then((_) => right(unit));
      } on PlatformException catch (e) {
        if (e.code == 'ERROR_WRONG_PASSWORD' ||
            e.code == 'ERROR_USER_NOT_FOUND') {
          return left(const AuthFailure.invalidEmailAndPasswordCombination());
        }
        return left(const AuthFailure.serverError());
      }
    }
    ...
  }
  ```

- **CONSIDER** omitting the creation of `data sources` when using advanced client libraries like `cloud_firestore` and `firebase_auth`.

- **DO** perform catching logic to transform `exceptions` in `Failures` defined in the `domain` layer for returning `Either<Failure, Entity>` results.

### 7.3. State Management

- **DO** use [flutter_bloc](https://pub.dev/packages/flutter_bloc) to implement the BLoC (Business Logic Component) design pattern for state management.


- **DO** use `BLoC` instead of `Cubit`.

### 7.4. Widgets

- **DO** look for patterns of duplicate code and refactor it out to a widget. Refactoring widgets is just like refactoring standard code. Widgets can be incredibly small but if used throughout the app they both reduce code and make it easier to apply changes later on.

  For example create an `ElevatedButton` widget which wraps a standard `RaisedButton` class so it always has a consistent elevation and enables setting the color or applying an icon.

- **DO** follow the convention where the name of the widget is the concatenation of the child widgets it combines.

- **DO** use an `AppWidget` to support changing top level settings such as the application theme or locale by reacting in a `MultiBlocProvider` to a `ThemeBloc` and a `SettingsBloc` to trigger a rebuild of the `MaterialApp` without affecting the `state`.

## 8. Testing

A well-tested app has many unit and widget tests, tracked by code coverage, plus enough integration tests to cover all the important use cases.

- **DO** use `unit tests` for testing single functions, methods, or classes. The goal of a unit test is to verify the correctness of a unit of logic under a variety of conditions.

- **DO** use `widget tests` to test single widget behavior. The goal of a widget test is to verify that the widget’s UI looks and interacts as expected.

- **DO** use `integration tests` for testing the critical flows of the entire app. The goal of an integration test is to verify that all the widgets and services being tested work together as expected, including the app performance.

- **DO** use [Mockito](https://flutter.dev/docs/cookbook/testing/unit/mocking) to mock live web services or databases.

- **DO** use [bloc_test](https://pub.dev/packages/bloc_test) for testing BLoC state management.

- **DO** use [get_it](https://pub.dev/packages/get_it) and [injectable](https://pub.dev/packages/injectable) to easily switch the implementation to a mocked version.

## 9. Security

In addition to being robust against chaotic users and errors in other systems, the code also has to withstand attacks from people who really know the nuts and bolts of your programming languages, your frameworks, and your deployment platform.

- **DO** consider security from the beginning, not as an afterthought.

- **DO** add security unit tests, including authentication and roles enforcement, injection prevention, escaping, sanitizing and validating all the inputs.

- **DO** keep current with the latest Flutter SDK releases.

- **DO** keep dependencies up to date. Make sure to [upgrade your package dependencies](https://flutter.dev/docs/development/tools/sdk/upgrading) to keep the dependencies up to date.

- **AVOID** pinning to specific versions for dependencies and, if needed, make sure you check periodically to see if the dependencies have had security updates, and update the pin accordingly.

- **DO** document in the `README.md` file of the project if a dependency needs to be pinned ta an specific version with a comment explaining the reasons for that.

- **DO** use strong encryption everywhere data is transmitted or stored.

  For example:

  - use TLS for all communication with external services.
  
  - Store any credentials or secrets with [flutter_secure_storage](https://pub.dev/packages/flutter_secure_storage).
  
  - When using `flutter_secure_storage` keep application backup enabled but exclude the shared pref used by this `flutter_secure_storage` to avoid [unwrap key exceptions](https://github.com/mogol/flutter_secure_storage/issues/43#issuecomment-471642126).
  
  - Store data with [hydrated_bloc](https://pub.dev/packages/hydrated_bloc) using [Hive encrypted box](https://docs.hivedb.dev/#/advanced/encrypted_box) and store the encryption key securely with [flutter_secure_storage](https://pub.dev/packages/flutter_secure_storage).

- **DO** clean any sensitive data from cached memory when not needed any more.

  Flutter provides the information when the application is about to switch to background mode. You can access the information to set up a timer for regular clean-ups of the cache with the permission of the user.

  ```dart
  class CacheControl extends WidgetsBindingObserver {
    CacheControl() {
      WidgetsBinding.instance.addObserver(this);
    }
    
    void _cleanAllCache() {
      // clean...
    }

    @override
    void didChangeAppLifecycleState(AppLifecycleState state) {
      if (sate == AppLifecycleState.paused) {
        const Future.delayed(
          const Duration(minutes: 15),
          _cleanAllCache,
        );
      }
    }
  }
  ```

- **DO** manage the app screenshots to control the exposure of sensitive data.

- **DO** look behind you.

  (-(-\_(-\_-)_-)-)

  ---
  
